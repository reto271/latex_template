# Manually configured
TikzOnly := Document_tikz_only

# Find files in directory
InputFiles := $(wildcard *.tex)
OutputFiles := $(patsubst %.tex,%.pdf,$(InputFiles))


.PHONY : all
all : $(OutputFiles) $(TikzOnly).svg
#	@echo "InputFiles : " $(InputFiles)
#	@echo "OutputFiles: " $(OutputFiles)
#	@echo "navFiles   : " $(navFiles)
	@echo done


${DocName}.pdf : $(DocName).tex
	pdflatex --file-line-error --synctex=1 --shell-escape ${DocName}.tex
	pdflatex --file-line-error --synctex=1 --shell-escape ${DocName}.tex


$(TikzOnly).svg : $(TikzOnly).pdf
	pdf2svg $(TikzOnly).pdf $(TikzOnly).svg
	convert $(TikzOnly).svg $(TikzOnly).png


.PHONY : clean
clean : $(FilesNoExtension)
	@$(RM) -fv $(auxFiles)
	@$(RM) -fv $(navFiles)
	@$(RM) -fv $(outFiles)
	@$(RM) -fv $(logFiles)
	@$(RM) -fv $(tocFiles)
	@$(RM) -fv $(vrbFiles)
	@$(RM) -fv $(logFiles)
	@$(RM) -fv $(lofFiles)
	@$(RM) -fv $(lotFiles)
	@$(RM) -fv $(snmFiles)
	@$(RM) -fv $(mwFiles)
	@$(RM) -fv $(gzFiles)
	@touch $(InputFiles)
	@$(RM) -fv $(TikzOnly).svg
	@$(RM) -fv $(TikzOnly).png

.PHONY : clean_all
clean_all : clean
	@$(RM) -fv $(OutputFiles)
	@$(RM) -fv *~
	@$(RM) -fv Makefile~


.PHONY : evince
evince :
	@evince $(OutputFiles)&


.PHONY : emacs
emacs :
	@emacs $(InputFiles)&


.PHONY : view
view :
	@eog $(TikzOnly).svg


%.pdf : %.tex
	@echo "target rule: $@"
	@echo "input rule: $^"
	pdflatex --file-line-error --synctex=1 --shell-escape $<
	pdflatex --file-line-error --synctex=1 --shell-escape $<


auxFiles := $(patsubst %.tex,%.aux,$(InputFiles))
navFiles := $(patsubst %.tex,%.nav,$(InputFiles))
outFiles := $(patsubst %.tex,%.out,$(InputFiles))
logFiles := $(patsubst %.tex,%.log,$(InputFiles))
tocFiles := $(patsubst %.tex,%.toc,$(InputFiles))
vrbFiles := $(patsubst %.tex,%.vrb,$(InputFiles))
logFiles := $(patsubst %.tex,%.log,$(InputFiles))
lofFiles := $(patsubst %.tex,%.lof,$(InputFiles))
lotFiles := $(patsubst %.tex,%.lot,$(InputFiles))
snmFiles := $(patsubst %.tex,%.snm,$(InputFiles))
mwFiles := $(patsubst %.tex,%.mw,$(InputFiles))
gzFiles := $(patsubst %.tex,%.synctex.gz,$(InputFiles))

#-autopp.*
#*.vrb
#	@rm -rfv _minted-${DocName}
